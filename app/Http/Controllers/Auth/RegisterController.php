<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    public function index()
    {
        return view('auth.register');
    }

    public function register(Request $request)
    {
        $this->validate(request(), [
            'cedula'                => 'required|unique:users,identityCard|numeric',
            'nombre'                => 'required|string|max:255',
            'correo'                 => 'required|string|email|max:255|unique:users,email',
            'usuario'               => 'required|string|unique:users,username',
            'password'              => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);

        $user = new User;
        $user->identityCard = $request->post('cedula');
        $user->name = $request->post('nombre');
        $user->email = $request->post('correo');
        $user->username = $request->post('usuario');
        $user->password = bcrypt($request->post('password'));
        $user->role_id = 3;
        if ($user->save()) {
            return redirect(route('log'))->with('flash', 'El usuario fue registrado exitosamente');
        }

        return back()->withErrors()
                     ->withInput(request()->all());
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('log');
    }
}
