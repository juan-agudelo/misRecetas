<?php

namespace App\Http\Controllers;

use App\Classification;
use Validator;
use Illuminate\Http\Request;

class ClassificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            $active = "classifications";
            return view('classifications.index', compact('active')); 
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            return view('classifications.create');
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            $validation = Validator::make($request->all(), [
                'clasificacion'  => 'required|unique:classifications,name'
            ]);

            $error_array = array();
            $success_output = "";

            if ($validation->fails()) {
                foreach ($validation->messages()->getMessages() as $field_name => $message) {
                    $error_array[] = $message;
                }
            } else {
                $classification = new Classification;
                $classification->name = $request->post('clasificacion');
                $classification->save();

                $success_output = "Nueva clasificacion creada";
            }

            return Response()->json([
                'error'   => $error_array,
                'success' => $success_output
            ]);
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            $classifications = Classification::all();
            return view('classifications.list', compact('classifications'));
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            // get the nerd
            $classification = Classification::find($id);

            // show the edit form and pass the nerd
            return view('classifications.edit', compact('classification'));
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            $id = $request->post('id');

            $validation = Validator::make($request->all(), [
                'clasificacion'  => "required|unique:classifications,name,$id,id"
            ]);

            $error_array = array();
            $success_output = "";

            if ($validation->fails()) {
                foreach ($validation->messages()->getMessages() as $field_name => $message) {
                    $error_array[] = $message;
                }
            } else {
                $classification = Classification::find($id);
                $classification->name = $request->post('clasificacion');
                $classification->save();

                $success_output = "Cambios guardados";
            }

            return Response()->json([
                'error'   => $error_array,
                'success' => $success_output
            ]);
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            $delete = Classification::find($request->post('id'))->delete();
            return Response()->json($delete);
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }
}
