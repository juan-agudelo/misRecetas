<?php

namespace App\Http\Controllers;

use App\Recipe;
use App\Ingredient;
use Validator;
use Illuminate\Http\Request;

class RecipeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active = "recipes";
        return view('recipes.index', compact('active'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ingredients = Ingredient::all();
        return view('recipes.create', compact('ingredients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $validation = Validator::make($request->all(), [
            'nombre'  => 'required',
            'descripcion'  => 'required',
            'imagen'  => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'id.*' => 'required',
            'cantidad.*' => 'required',
            'unidad.*' => 'required'
        ]);

        $error_array = array();
        $success_output = "";

        if ($validation->fails()) {
            foreach ($validation->messages()->getMessages() as $field_name => $message) {
                $error_array[] = $message;
            }
        } else {
            if($request->hasFile('imagen')) {
                $file = $request->file('imagen');
                $fileName = time().$file->getClientOriginalName();
                $file->move(public_path().'/img/recetas', $fileName);
            }

            $recipe = new Recipe;
            $recipe->name = $request->post('nombre');
            $recipe->description = $request->post('descripcion');
            $recipe->image = $fileName;
            $recipe->user_id = auth()->user()->role->id;
            $recipe->save();

            // Ingresar datos de la tabla intermedia.
            for ($i=0; $i < count($request->post('idIng')); $i++) { 
                $recipe->ingredients()->attach($request->post('idIng')[$i], ['quantity'=>$request->post('cantidad')[$i], 'unit'=>$request->post('unidad')[$i]]);
            }
            
            $success_output = "Nuevo ingredinate creado";
        }

        return Response()->json([
            'error'   => $error_array,
            'success' => $success_output,
            'data'    => $request->all()
        ]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {   
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            $recipes = Recipe::all();
        } else {
            $recipes = Recipe::where('user_id', auth()->user()->id);
        }
        return view('recipes.list', compact('recipes'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recipe = Recipe::find($id);
        return view('recipes.show', compact('recipe'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $recipe = Recipe::find($id);
        $ingredients = Ingredient::all();

        $pivot_ids = array();
        foreach ( $recipe->ingredients as $ingredient) {
            $pivot_ids[] = $ingredient->id;
        }

        return view('recipes.edit', compact('recipe', 'ingredients', 'pivot_ids'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {   
        $id = $request->post('id');

        $validation = Validator::make($request->all(), [
            'nombre'  => 'required',
            'descripcion'  => 'required',
            'imagen'  => 'image|mimes:jpeg,png,jpg|max:2048',
            'id.*' => 'required',
            'cantidad.*' => 'required',
            'unidad.*' => 'required'
        ]);

        $error_array = array();
        $success_output = "";

        if ($validation->fails()) {
            foreach ($validation->messages()->getMessages() as $field_name => $message) {
                $error_array[] = $message;
            }
        } else {
            $recipe = Recipe::find($id);

            if($request->hasFile('imagen')) {
                $file = $request->file('imagen');
                $fileName = time().$file->getClientOriginalName();
                $file->move(public_path().'/img/recetas', $fileName);
                if(file_exists(public_path('img/recetas/'.$recipe->image))){
                  unlink(public_path('img/recetas/'.$recipe->image));
                }
                $recipe->image = $fileName;
            }
            $recipe->name = $request->post('nombre');
            $recipe->description = $request->post('descripcion');
            $recipe->save();

            // Almacenar los ids 'post' en un array.
            $ids = array();
            for ($i=0; $i < count($request->post('idIng')); $i++) {
                $ids[] = $request->post('idIng')[$i];
            }

            // Almacenar los pivot_ids 'base de datos' en un array.
            $pivot_ids = array();
            foreach ( $recipe->ingredients as $ingredient) {
                $pivot_ids[] = $ingredient->id;
            }

            // Encontrar los ids que no estan en la base de datos para agregarlos a la misma.
            $resultado = array_diff($ids, $pivot_ids);

            // Agregar ingredientes a la tabla intermedia.
            while (current($resultado)) {
                $input = key($resultado);
                $recipe->ingredients()->attach($request->post('idIng')[$input], ['quantity'=>$request->post('cantidad')[$input], 'unit'=>$request->post('unidad')[$input]]);
                next($resultado);
            }



            foreach ($recipe->ingredients as $ingredient) {
                // Actualizar ingredientes en la tabla intermedia.
                if(in_array($ingredient->id, $ids) !== false) {

                    while ($input = current($ids)) {
                        if ($input == $ingredient->id) {
                            $indice_input = key($ids);
                            $recipe->ingredients()->updateExistingPivot($request->post('idIng')[$indice_input], ['quantity'=>$request->post('cantidad')[$indice_input], 'unit'=>$request->post('unidad')[$indice_input]]);
                        }
                        next($ids);
                    }

                } 
                // Eliminar ingredientes en la tabla intermedia
                else {
                    $recipe->ingredients()->detach($ingredient->id);
                }
            }
            
            $success_output = "Receta editada";
            $result = true;
        }

        return Response()->json([
            'error'   => $error_array,
            'success' => $success_output
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $recipe = Recipe::find($request->post('id'));
        foreach ($recipe->ingredients as $ingredient) {
            $recipe->ingredients()->detach($ingredient->id);
        }
        $recipe->delete(); // Borramos el post
        return Response()->json($recipe);
    }
}
