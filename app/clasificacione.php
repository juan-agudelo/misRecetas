<?php

namespace MisRecetas;

use Illuminate\Database\Eloquent\Model;

class Clasificacione extends Model
{
	protected $table = 'CLASIFICACIONES';
	protected $primaryKey = 'cla_id';
	public $timestamps = false;

	public function ingrediente()
    {
    	return $this->hasMany('MisRecetas\Ingrediente');
    }
}