<div class="modal" role="dialog" id="modal-action">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Detalles del ingrediente</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="card text-center">
					<img class="card-img-top" src="{{ asset('img/ingredientes/'.$ingredient->image) }}" alt="Card image cap">
					<div class="card-body p-0">
						<div class="row m-0">
							<div class="col-8 p-2 bg-light">
								<h5 class="card-title">{{ $ingredient->name }}</h5>
								<p class="card-text text-justify">{{ $ingredient->description }}</p>
							</div>
							<div class="col-4 p-2 bg-info d-flex align-items-center justify-content-center">
								<span class="text-white">{{ $ingredient->classification->name }}</span>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>