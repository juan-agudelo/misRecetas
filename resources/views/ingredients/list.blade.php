@if(count($ingredients) > 0)
	{{ $cont = 1}} 
	@foreach($ingredients as $ingredient)
		<tr>
			<th scope="row">{{ $cont++ }}</th>
			<td>{{ $ingredient->name }}</td>
			<td>{!! (strlen($ingredient->description)>35) ? substr($ingredient->description,0,35)."..." : $ingredient->description !!}</td>
			<td>{{ $ingredient->classification->name }}</td>
			<td>
				<button type="button" class="btn btn-info" id="btn-show" data-id="{{ $ingredient->id }}">Detalles</button>
				<button type="button" class="btn btn-warning" id="btn-edit" data-id="{{ $ingredient->id }}">Editar</button>
				<button type="button" class="btn btn-danger" id="btn-delete" data-id="{{ $ingredient->id }}" data-token="{{ csrf_token() }}">Eliminar</button>
			</td>
		</tr>
	@endforeach
@else
<tr>
	<td colspan="5">No se han encontrado ingredientes.</td>
</tr>
@endif