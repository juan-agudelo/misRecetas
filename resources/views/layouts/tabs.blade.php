<ul class="nav nav-tabs mt-5">
	<li class="nav-item">
		<a class="nav-link @if($active == 'inicio') active @endif" href="{{ route('home') }}">Inicio</a>
	</li>
	@if (auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') 
		<li class="nav-item">
			<a class="nav-link @if($active == 'users') active @endif" href="{{ route('usuario.index') }}">Usuarios</a>
		</li>
	@endif
	@if (auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') 
	<li class="nav-item">
		<a class="nav-link @if($active == 'classifications') active @endif" href="{{ route('clasificacion.index') }}">Clasificaciones</a>
	</li>
	@endif
	@if (auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') 
	<li class="nav-item">
		<a class="nav-link @if($active == 'ingredients') active @endif" href="{{ route('ingrediente.index') }}">Ingredientes</a>
	</li>
	@endif
	<li class="nav-item">
		<a class="nav-link @if($active == 'recipes') active @endif" href="{{ route('receta.index') }}">Recetas</a>
	</li>
</ul>