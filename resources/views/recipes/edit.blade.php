<div class="modal" role="dialog" id="modal-action">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST" action="{{ route('receta.update') }}" class="needs-validation" id="form" data-action="edit" enctype="multipart/form-data" novalidate>
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Nueva receta</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}
					<div id="alert"></div>
					<div class="form-group">
						<label for="nombre">nombre:</label>
						<input type="text" class="form-control" name="nombre"  id="nombre" placeholder="Por favor ingrese un nombre" value="{{ $recipe->name }}" required>
						<div class="invalid-feedback">Por favor, ingrese un nombre.</div>
					</div>
					<div class="form-group">
						<label for="descripcion">Descripción:</label>
						<textarea class="form-control" name="descripcion" id="descripcion" rows="3" required>{{ $recipe->description }}</textarea>
						<div class="invalid-feedback">Por favor, ingrese una descripción.</div>
					</div>
					<div class="form-group">
						<label for="imagen">Imagen:</label>
						<input type="file" class="form-control" name="imagen" id="imagen" placeholder="Por favor seleccione una imagen" accept="image/x-png,image/jpg,image/jpeg">
						<div class="invalid-feedback">Por favor, Seleccione una imagen.</div>
					</div>
					<div class="border py-1">
						<div class="form-row">
							<div class="form-group col-md-8 pl-2">
								<label for="ingredientes">Ingredientes:</label>
								<select class="form-control select-search" id="selectIngredient">
									@if(count($ingredients) > 0)
										<option value="">Seleccione un ingrediente</option>
										@foreach($ingredients as $ingredient)
											@if(in_array($ingredient->id, $pivot_ids, true) !== false)
											@else
												<option value="{{ $ingredient->id }}">{{ $ingredient->name }}</option>
											@endif
										@endforeach
									@else
										<option value="">No hay clasificaciones para seleccionar</option>
									@endif
								</select>
							</div>
							<div class="form-group col-md-4 d-flex align-items-end pr-2">
								<button type="button" class="btn btn-success btn-block" id="addInputs">Agregar</button>
							</div>
						</div>
						<div class="table-responsive px-2">
							<table class="table" id="itemTable">
								<tr>
									<th>Nombre</th>
									<th>Cantidad</th>
									<th>Unidad y conector</th>
									<th></th>
								</tr>
								@foreach($recipe->ingredients as $ingredient)
									<tr>
										<td>
											<span>{{ $ingredient->name }}</span>
											<input type="hidden" class="form-control" name="idIng[]" value="{{ $ingredient->id }}">
										</td>
										<td>
											<input type="number" class="form-control" min="1" name="cantidad[]" value="{{ $ingredient->pivot->quantity }}" required>
											<div class="invalid-feedback">Por favor, ingrese una cantidad.</div>
										</td>
										<td>
											<input type="text" class="form-control" name="unidad[]" value="{{ $ingredient->pivot->unit }}" required>
											<div class="invalid-feedback">Por favor, ingrese una unidad.</div>
										</td>
										<td>
											<button type="button" class="btn btn-danger btn-block removeInput">X</button>
										</td>
									</tr>
								@endforeach
							</table>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="id" value="{{ $recipe->id }}">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="action-modal btn btn-primary" id="submit">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>