@extends('layouts.app')

@section('title', 'Registro')

@section('content')
	
	<div class="row d-flex justify-content-center mt-3">
		<div class="col-md-6 col-md-offset-3">
			<div class="card">
				<div class="card-header text-center" >
					<h1 class="card-title">Registro de usuario</h1>
				</div>
				<div class="card-body">

					<form method="POST" action="{{ route('register') }}">
						{{ csrf_field() }}
						<div class="form-group">
							<label for="cedula">Cedula:</label>
							<input type="number" class="form-control number {{ $errors->has('cedula') ? 'is-invalid' : '' }}" name="cedula"  id="cedula" placeholder="Por favor ingrese el numero de cedula" value="{{ old('cedula') }}">
							{!! $errors->first('cedula', '<small id="emailHelp" class="form-text text-danger">:message</small>') !!}
						</div>
						<div class="form-group">
							<label for="nombre">Nombre completo:</label>
							<input type="text" class="form-control {{ $errors->has('nombre') ? 'is-invalid' : '' }}" name="nombre" id="nombre" placeholder="Por favor ingrese los nombres y apellidos" value="{{ old('nombre') }}">
							{!! $errors->first('nombre', '<small id="emailHelp" class="form-text text-danger">:message</small>') !!}
						</div>
						<div class="form-group">
							<label for="correo">Correo electrónico:</label>
							<input type="email" autocomplete='email' class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="correo" id="correo" placeholder="Por favor ingrese el correo electronico" value="{{ old('email') }}">
							{!! $errors->first('email', '<small id="emailHelp" class="form-text text-danger">:message</small>') !!}
						</div>
						<div class="form-group">
							<label for="usuario">Usuario:</label>
							<input type="text" class="form-control {{ $errors->has('usuario') ? 'is-invalid' : '' }}" name="usuario" id="usuario" placeholder="Por favor ingrese un nombre de usuario" value="{{ old('usuario') }}">
							{!! $errors->first('usuario', '<small id="emailHelp" class="form-text text-danger">:message</small>') !!}
						</div>
						<div class="form-group">
							<label for="password">Contraseña:</label>
							<input type="text" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" name="password" id="password" placeholder="Por favor ingrese un nombre de usuario">
							{!! $errors->first('password', '<small id="emailHelp" class="form-text text-danger">:message</small>') !!}
						</div>
						<div class="form-group">
							<label for="password_confirmation">Confirmar contraseña:</label>
							<input type="text" class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}" name="password_confirmation" id="password_confirmation" placeholder="Por favor ingrese un nombre de usuario">
							{!! $errors->first('password_confirmation', '<small id="emailHelp" class="form-text text-danger">:message</small>') !!}
						</div>
						<button type="submit" class="btn btn-primary btn-lg btn-block">Registrar</button>
					</form>
					
				</div>
			</div>
		</div>
	</div>

@endsection